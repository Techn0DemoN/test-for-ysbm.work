<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProductInformationController extends Controller
{
    /**
     * @Route("/full_info/{id}", name="app_product_full_information", requirements={"id" = "\d+"})
     * @Security("is_granted('ROLE_USER')")
     * @Template()
     */
    public function indexAction($id)
    {   
        $product = $this->getDoctrine()->getRepository('AppBundle:Product')->findOneById($id);
        
        return ['product' => $product];     
    }
}
