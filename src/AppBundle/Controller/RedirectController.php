<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RedirectController extends Controller
{
    /**
     * @Route("/redirect/login", name="redirect_login")
     * @Template()
     */
    public function indexAction()
    {
        $user = $this->getUser();
        if ($user) 
        {
            foreach($user->getRoles() as $role)
            {
                switch ($role)
                {
                    case 'ROLE_SUPER_ADMIN': {
                        return $this->redirect($this->generateUrl('sonata_admin_dashboard'), 301);
                        break;    
                    }
                    case 'ROLE_USER': {
                        return $this->redirect($this->generateUrl('user_product'), 301);
                        break;    
                    }
                    default: {
                        return $this->redirect($this->generateUrl('homepage'), 301);
                        break;                         
                    }
                } 
            }
        }       
    }
}
