<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserProductController extends Controller
{
    /**
     * @Route("/user_product", name="user_product")
     * @Template()
     * @Security("is_granted('ROLE_USER')")
     */
    public function indexAction()
    {    
        $productSet = $this->getDoctrine()->getRepository('AppBundle:ProductSet')->findBy(array('visible' => '1'));
        $stmt = $this->getDoctrine()->getManager()->getConnection()
            ->prepare('
                SELECT GROUP_CONCAT(`set_with_product`.`set_id`) as id FROM `product` 
                LEFT JOIN `set_with_product` ON `product`.`id` = `set_with_product`.`product_id`
                WHERE `product`.`visible` = 0 AND `set_with_product`.`set_id`IS NOT null
            ');
        $stmt->execute();
        $idArray = $stmt->fetchAll();
        $idArray = explode(',', $idArray[0]['id']);
        $filteredSet = array();
        foreach($productSet as $set)
        {
            if (!in_array ( $set->getId() ,  $idArray )){
                $filteredSet[] = $set;   
            }    
        }
        
        $product = $this->getDoctrine()->getRepository('AppBundle:Product')->findBy(array('visible' => '1'));
        $stmt = $this->getDoctrine()->getManager()->getConnection()
            ->prepare('
                SELECT GROUP_CONCAT(`product`.`id`) as id FROM `product`
                LEFT JOIN `set_with_product` ON `product`.`id` = `set_with_product`.`product_id`
                WHERE `set_with_product`.`set_id` IS NULL
            ');
        $stmt->execute();
        $idArray = $stmt->fetchAll();
        $idArray = explode(',', $idArray[0]['id']);
        $filteredProduct = array();
        foreach($product as $prod)
        {
            if (in_array ( $prod->getId() ,  $idArray )){
                $filteredProduct[] = $prod; 
            }    
        }        
   
        return ['productSet' => $filteredSet, 
                'productOnly' => $filteredProduct,
                ];
    }
}
