<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ProductAdmin extends Admin
{

    protected $baseRouteName = 'sonata_product';
    protected $baseRoutePattern = 'product';

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, [
                'label' => 'app.product.labels.name',
              ])
            ->add('visible', null , [
                'label' => 'app.product.labels.visible',
              ])
            ->add('description', null, [
                'label' => 'app.product.labels.description',
              ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, [
                'label' => 'app.product.labels.name',
              ])
            ->add('description', null, [
                'label' => 'app.product.labels.description',
              ])
            ->add('visible', null , [
                'label' => 'app.product.labels.visible',
              ])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, [
                'label' => 'app.product.labels.name',
              ])
            ->add('productDetail', null, [
                'label' => 'app.product.labels.productDetail',
                'required' => false,
              ])
            ->add('visible', null , [
                'label' => 'app.product.labels.visible',
              ])
            ->add('description', 'ckeditor', [
                'label' => 'app.product.labels.description',
                'config' => [
                    'filebrowserUploadRouteParameters' => [],
                ]
              ])
            ->add('image', 'iphp_file', [
                'label' => 'app.product.labels.image',
              ])
        ;
    }
}