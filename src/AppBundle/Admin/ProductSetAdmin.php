<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ProductSetAdmin extends Admin
{

    protected $baseRouteName = 'sonata_product_set';
    protected $baseRoutePattern = 'product_set';

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, [
                'label' => 'app.product_set.labels.name',
              ])
            ->add('visible', null , [
                'label' => 'app.product_set.labels.visible',
              ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, [
                'label' => 'app.product_set.labels.name',
              ])
            ->add('visible', null , [
                'label' => 'app.product_set.labels.visible',
              ])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, [
                'label' => 'app.product_set.labels.name',
              ])
            ->add('product', null, [
                'label' => 'app.product_set.labels.product',
                'required' => false,
              ])
            ->add('visible', null , [
                'label' => 'app.product_set.labels.visible',
              ])
        ;
    }
}