<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class UserAdmin extends Admin
{

    protected $baseRouteName = 'sonata_user';
    protected $baseRoutePattern = 'user';

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username', null, [
                'label' => 'app.user.labels.username',
              ])
            ->add('email', null, [
                'label' => 'app.user.labels.email',
              ])
            ->add('enabled', null, [
                'label' => 'app.user.labels.enabled',
              ])
            ->add('locked', null, [
                'label' => 'app.user.labels.locked',
              ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('username', null, [
                'label' => 'app.user.labels.username',
              ])
            ->add('email', null, [
                'label' => 'app.user.labels.email',
              ])
            ->add('enabled', null, [
                'editable' => true,
                'label' => 'app.user.labels.enabled',
              ])
            ->add('locked', null, [
                'editable' => true,
                'label' => 'app.user.labels.locked',
              ])
            ->add('lastLogin', null, [
                'label' => 'app.user.labels.last_login',
              ])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $roles = [];
        foreach (array_keys($this->getConfigurationPool()->getContainer()->getParameter('security.role_hierarchy.roles')) as $role) {
            $roles[$role] = $role;
        }

        $formMapper
            ->add('username', null, [
                'label' => 'app.user.labels.username',
              ])
            ->add('email', null, [
                'label' => 'app.user.labels.email',
              ])
            ->add('enabled', null, [
                'label' => 'app.user.labels.enabled',
                'required' => false,
              ])
            ->add('locked', null, [
                'label' => 'app.user.labels.locked',
                'required' => false,
              ])
            ->add('roles', 'choice', [
                'label' => 'app.user.labels.user_role',
                'choices' => $roles,
                'multiple' => true,
                'required' => false,
            ])
            ->add('plainPassword', 'password', [
                'label' => 'app.user.labels.plain_password',
                'required' => false,
              ])
        ;
    }

    public function preUpdate($user)
    {
        if ($user->getPlainPassword() != "") {
            $user->setPassword($user->getPlainPassword());
        }
    }
    
    public function prePersist($user) 
    {   
        $container = $this->getConfigurationPool()->getContainer();
        $email = $container->getParameter('mailer_user');
        $message = \Swift_Message::newInstance()
            ->setSubject('Hello new password')
            ->setFrom($email)
            ->setTo($user->getEmail())
            ->setBody("Your login - {$user->getUsername()} , password-{$user->getPlainPassword()}");
        $container->get('mailer')->send($message);
      
    }

}
