<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ProductDetailAdmin extends Admin
{

    protected $baseRouteName = 'sonata_product_detail';
    protected $baseRoutePattern = 'product_detail';

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, [
                'label' => 'app.product_detail.labels.name',
              ])
            ->add('description', null, [
                'label' => 'app.product_detail.labels.description',
              ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, [
                'label' => 'app.product_detail.labels.name',
              ])
            ->add('description', null, [
                'label' => 'app.product_detail.labels.description',
              ])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, [
                'label' => 'app.product_detail.labels.name',
              ])
            ->add('description', null, [
                'label' => 'app.product_detail.labels.description',
              ])
        ;
    }
}