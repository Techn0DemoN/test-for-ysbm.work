<?php
namespace AppBundle\Entity;
use Iphp\FileStoreBundle\Mapping\Annotation as FileStore;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @FileStore\Uploadable
 * @ORM\Table(name="product")
 * @ORM\Entity
 */
class Product
{   
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=512)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    
    /**
     * @var array
     * //@Assert\File( maxSize="2M")
     * @FileStore\UploadableField(mapping="products_photo")
     * @ORM\Column(name="image", type="array", nullable=true)
     */
    private $image;
    
    /**
     * @ORM\ManyToMany(targetEntity="ProductDetail")
     * @ORM\JoinTable(name="product_with_detail",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="detail_id", referencedColumnName="id")}
     *      )
     */
    private $productDetail;
    
    /**
     * @var integer
     * @ORM\Column(name="visible", type="boolean", nullable=true, options={"default" = 0})
     */
    private $visible;  
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productDetail = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set image
     *
     * @param array $image
     *
     * @return Product
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return array
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return Product
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Add productDetail
     *
     * @param \AppBundle\Entity\ProductDetail $productDetail
     *
     * @return Product
     */
    public function addProductDetail(\AppBundle\Entity\ProductDetail $productDetail)
    {
        $this->productDetail[] = $productDetail;

        return $this;
    }

    /**
     * Remove productDetail
     *
     * @param \AppBundle\Entity\ProductDetail $productDetail
     */
    public function removeProductDetail(\AppBundle\Entity\ProductDetail $productDetail)
    {
        $this->productDetail->removeElement($productDetail);
    }

    /**
     * Get productDetail
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductDetail()
    {
        return $this->productDetail;
    }
}
