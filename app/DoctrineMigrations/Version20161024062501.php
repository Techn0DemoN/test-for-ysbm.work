<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161024062501 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('
           
            SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
            SET time_zone = "+00:00";
            
            
            /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
            /*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
            /*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
            /*!40101 SET NAMES utf8mb4 */;
            
            --
            -- База данных: `ysbm`
            --
            
            -- --------------------------------------------------------
            
            --
            -- Структура таблицы `fos_user`
            --
            
            CREATE TABLE IF NOT EXISTS `fos_user` (
              `id` int(11) NOT NULL,
              `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `enabled` tinyint(1) NOT NULL,
              `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `last_login` datetime DEFAULT NULL,
              `locked` tinyint(1) NOT NULL,
              `expired` tinyint(1) NOT NULL,
              `expires_at` datetime DEFAULT NULL,
              `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `password_requested_at` datetime DEFAULT NULL,
              `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT \'(DC2Type:array)\',
              `credentials_expired` tinyint(1) NOT NULL,
              `credentials_expire_at` datetime DEFAULT NULL
            ) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
            
            --
            -- Дамп данных таблицы `fos_user`
            --
            
            INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`) VALUES
            (17, \'admin\', \'admin\', \'bondarenko.roman@mail.ru\', \'bondarenko.roman@mail.ru\', 1, \'ttn4p445bkgc40g0cggk4kscg0cc8s0\', \'$2y$13$ttn4p445bkgc40g0cggk4esfcroaZTEDSCBkS7e2mfL21LB93Pnf.\', \'2016-10-24 03:22:59\', 0, 0, NULL, NULL, NULL, \'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}\', 0, NULL),
            (18, \'user\', \'user\', \'bondarenko.roman.v@gmail.com\', \'bondarenko.roman.v@gmail.com\', 1, \'qu5ytvyd00gckg4skk4004w4wwck8sk\', \'$2y$13$qu5ytvyd00gckg4skk400upsZ6RJ8GqCJJ03dqPropJYk30es53yO\', \'2016-10-24 03:05:44\', 0, 0, NULL, \'8PVCRXRbERI7FVQ0gEKluaLSnl0wAexgx8xTjzIeQ40\', \'2016-10-21 03:43:54\', \'a:0:{}\', 0, NULL),
            (19, \'user1\', \'user1\', \'bondarenko.roman.v12@gmail.com\', \'bondarenko.roman.v12@gmail.com\', 1, \'py5bef3q7yoc4gg0s0wgcokkckkss8g\', \'$2y$13$py5bef3q7yoc4gg0s0wgceXNL4ZaYdZS71hSIw/D4ESC5LqXrK8kC\', NULL, 0, 0, NULL, NULL, NULL, \'a:0:{}\', 0, NULL);
            
            -- --------------------------------------------------------
            
            --
            -- Структура таблицы `migration_versions`
            --
            
            CREATE TABLE IF NOT EXISTS `migration_versions` (
              `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
            
            -- --------------------------------------------------------
            
            --
            -- Структура таблицы `product`
            --
            
            CREATE TABLE IF NOT EXISTS `product` (
              `id` int(11) NOT NULL,
              `name` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
              `description` longtext COLLATE utf8_unicode_ci,
              `image` longtext COLLATE utf8_unicode_ci COMMENT \'(DC2Type:array)\',
              `visible` tinyint(1) DEFAULT \'0\'
            ) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
            
            --
            -- Дамп данных таблицы `product`
            --
            
            INSERT INTO `product` (`id`, `name`, `description`, `image`, `visible`) VALUES
            (6, \'Тесто для пиццы\', \'<p>Хрустящее тесто для пиццы</p>\', \'a:7:{s:8:"fileName";s:64:"/140601161438-140607163114-p-O-hrustjashhee-testo-dlja-picci.jpg";s:12:"originalName";s:63:"140601161438-140607163114-p-O-hrustjashhee-testo-dlja-picci.jpg";s:8:"mimeType";s:10:"image/jpeg";s:4:"size";i:18060;s:4:"path";s:70:"/photo/140601161438-140607163114-p-O-hrustjashhee-testo-dlja-picci.jpg";s:5:"width";i:300;s:6:"height";i:300;}\', 1),
            (7, \'Настоящий греческий салат\', \'<p>Главный ингредиент салата хориатики &mdash; фета, традиционный греческий сыр из овечьего или козьего молока. По консистенции он напоминает плотный творог, а во вкусе имеет приятную ненавязчивую кислинку. В отличие от соленой и резкой брынзы, которой в этом салате часто заменяют фету остальные европейцы, он не забивает собой вкус свежих овощей, оливок и оливкового масла.</p>\', \'a:7:{s:8:"fileName";s:64:"/110808212803-130725151049-p-O-nastojaschij-grecheskij-salat.jpg";s:12:"originalName";s:63:"110808212803-130725151049-p-O-nastojaschij-grecheskij-salat.jpg";s:8:"mimeType";s:10:"image/jpeg";s:4:"size";i:21440;s:4:"path";s:70:"/photo/110808212803-130725151049-p-O-nastojaschij-grecheskij-salat.jpg";s:5:"width";i:300;s:6:"height";i:300;}\', 1),
            (8, \'Салат с манго и куриной печенью\', \'<p>Теплые салаты &mdash; сезонная штука, идеальная для ранней осени, когда еще не слишком холодно, чтобы мечтать о горячем рагу, и уже не так жарко, чтобы обходиться в обед салатником свежих овощей. Куриную печень не нужно долго жарить &mdash; всего трех-четырех минут на сковородке будет достаточно. А манго, если есть такая возможность, лучше купить заранее, за пару дней до использования, чтобы дать ему дома дозреть до идеальной кондиции.</p>\', \'a:7:{s:8:"fileName";s:65:"/110816121851-130725151830-p-O-salat-s-mango-kurinoj-pechenju.jpg";s:12:"originalName";s:64:"110816121851-130725151830-p-O-salat-s-mango-kurinoj-pechenju.jpg";s:8:"mimeType";s:10:"image/jpeg";s:4:"size";i:29121;s:4:"path";s:71:"/photo/110816121851-130725151830-p-O-salat-s-mango-kurinoj-pechenju.jpg";s:5:"width";i:300;s:6:"height";i:300;}\', 1),
            (9, \'Салат из лосося, сельдерея и моркови\', \'<p>Этот элементарный, но очень изящный салат нужно готовить только из лучших ингредиентов &mdash; свежезасоленного, лучше даже в домашних условиях, лосося, молодой сладкой моркови и ароматного хрустящего сельдерея. И заправлять лучшим, с ароматом только что скошенной травы оливковым маслом. А посыпать &mdash; исключительно свежемолотым черным перцем.</p>\', \'a:7:{s:8:"fileName";s:69:"/110816122019-120424122752-p-O-salat-iz-lososja-seldereja-morkovi.jpg";s:12:"originalName";s:68:"110816122019-120424122752-p-O-salat-iz-lososja-seldereja-morkovi.jpg";s:8:"mimeType";s:10:"image/jpeg";s:4:"size";i:31188;s:4:"path";s:75:"/photo/110816122019-120424122752-p-O-salat-iz-lososja-seldereja-morkovi.jpg";s:5:"width";i:300;s:6:"height";i:300;}\', 1),
            (10, \'Салат из капусты с креветками\', \'<p>Северный ответ салату из рукколы с креветками</p>\', \'a:7:{s:8:"fileName";s:64:"/130924160547-131002150505-p-O-salat-iz-kapusti-s-krevetkami.jpg";s:12:"originalName";s:63:"130924160547-131002150505-p-O-salat-iz-kapusti-s-krevetkami.jpg";s:8:"mimeType";s:10:"image/jpeg";s:4:"size";i:16871;s:4:"path";s:70:"/photo/130924160547-131002150505-p-O-salat-iz-kapusti-s-krevetkami.jpg";s:5:"width";i:300;s:6:"height";i:300;}\', 1),
            (11, \'Салат «Маса-Бармаса»\', \'<p>Салат из овощей и гребешков, изюминка которого в смеси японских специй, которой они щедро посыпаются</p>\', \'a:7:{s:8:"fileName";s:53:"/120214140918-131008104227-p-O-salat-masa-barmasa.jpg";s:12:"originalName";s:52:"120214140918-131008104227-p-O-salat-masa-barmasa.jpg";s:8:"mimeType";s:10:"image/jpeg";s:4:"size";i:41462;s:4:"path";s:59:"/photo/120214140918-131008104227-p-O-salat-masa-barmasa.jpg";s:5:"width";i:300;s:6:"height";i:300;}\', 1),
            (12, \'Оливье с лососем\', \'<p>Что тут говорить, самое обычное оливье с лососем. Вам понравится :)</p>\', \'a:7:{s:8:"fileName";s:50:"/151212225533-151222191857-p-O-olive-s-lososem.jpg";s:12:"originalName";s:49:"151212225533-151222191857-p-O-olive-s-lososem.jpg";s:8:"mimeType";s:10:"image/jpeg";s:4:"size";i:25191;s:4:"path";s:56:"/photo/151212225533-151222191857-p-O-olive-s-lososem.jpg";s:5:"width";i:300;s:6:"height";i:300;}\', 1),
            (13, \'Салат из телячьего языка\', \'<p>В этом салате настоятельно рекомендуется использовать азербайджанские овощные ингредиенты, особенно помидоры и огурцы. Именно они придают блюду концентрированный южный аромат. А язык может иметь любое происхождение. Лишь бы был свежесварен и исключительно нежен</p>\', \'a:7:{s:8:"fileName";s:61:"/150615095301-150617131140-p-O-salat-iz-teljachego-jazika.jpg";s:12:"originalName";s:60:"150615095301-150617131140-p-O-salat-iz-teljachego-jazika.jpg";s:8:"mimeType";s:10:"image/jpeg";s:4:"size";i:43444;s:4:"path";s:67:"/photo/150615095301-150617131140-p-O-salat-iz-teljachego-jazika.jpg";s:5:"width";i:300;s:6:"height";i:300;}\', 1);
            
            -- --------------------------------------------------------
            
            --
            -- Структура таблицы `product_detail`
            --
            
            CREATE TABLE IF NOT EXISTS `product_detail` (
              `id` int(11) NOT NULL,
              `name` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
              `description` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL
            ) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
            
            --
            -- Дамп данных таблицы `product_detail`
            --
            
            INSERT INTO `product_detail` (`id`, `name`, `description`) VALUES
            (3, \'Пшеничная мука\', \'самая обычная пшеничная мука\'),
            (4, \'Оливковое масло\', \'самое обычное оливковое масло\'),
            (5, \'Молоко\', \'самое обычное молоко\'),
            (6, \'Яйцо\', \'самое обычное куриное яйцо\'),
            (7, \'Соль\', \'самая обычная соль\'),
            (8, \'Помидоры\', \'самые обычные помидоры с грядки\'),
            (9, \'Зеленый перец\', \'самый обычный зеленый перец\'),
            (10, \'Маслины\', \'обычные маслины\'),
            (11, \'Огурцы\', \'обычные огурцы\'),
            (12, \'Сушеный орегано\', \'какой то непонятный орегано\'),
            (13, \'Сыр фетта\', \'в общем сыр\'),
            (16, \'Красный лук\', \'обычный красный лук\'),
            (17, \'Куриная печень\', \'Просто куриная печень\'),
            (18, \'Манго\', \'просто манго\'),
            (19, \'Смесь салатных листьев\', \'просто смесь каких то листьев\'),
            (20, \'Дижонская горчица\', \'просто дижонская горчица\'),
            (21, \'Мед\', \'простой мед\'),
            (22, \'Горчица\', \'просто горчица\'),
            (23, \'Молотый черный перец\', \'просто молотый черный перец\'),
            (24, \'Слабосоленый лосось\', \'простой слабосоленый лосось\'),
            (25, \'Сельдерей\', \'простой сельдерей\'),
            (26, \'Морковь\', \'простая морковь\'),
            (27, \'Чеснок\', \'обычный чеснок\'),
            (28, \'Белокочанная капуста\', \'обычная белокочанная капуста\'),
            (29, \'Креветки\', \'обычные кеветки\'),
            (30, \'Редис\', \'обычный редис\'),
            (31, \'Кислые яблоки\', \'обычные кислые яблоки\'),
            (32, \'Лук-порей\', \'обычный лук порей\'),
            (33, \'Уксус\', \'обычный уксус\'),
            (34, \'Сахар\', \'обычный сахар\'),
            (35, \'Растительное масло\', \'обычное растительное масло\'),
            (36, \'Приправа для моркови по корейски\', \'обычная приправа для моркови по корейски\'),
            (37, \'Черная соль\', \'странная черная соль\'),
            (39, \'Спаржа\', \'обычная спаржа\'),
            (40, \'Цукини\', \'обычные цукини\'),
            (41, \'Гребешки\', \'обычные гребешки\'),
            (42, \'Помидоры черри\', \'простые помидоры черри\'),
            (43, \'Проростки зеленого гороха\', \'простые проростки зеленого гороха\'),
            (44, \'Лимонный сок\', \'простой лимонный сок\'),
            (45, \'Приправа фурикакэ\', \'простая приправа фурикакэ\'),
            (46, \'Картофель\', \'простой картофель\'),
            (47, \'Консервированный зеленый горошек\', \'простой консервированный зеленый горошек\'),
            (49, \'Соленые огурцы\', \'простые соленые огурцы\'),
            (51, \'Слабосоленая семга\', \'простая слабосоленая семга\'),
            (52, \'Укроп\', \'простой укроп\'),
            (53, \'Майонез\', \'простой майонез\'),
            (54, \'Маринованный имбирь\', \'простой маринованный имбирь\'),
            (55, \'Зеленый лук\', \'простой зеленый лук\'),
            (56, \'Шампиньоны\', \'просто шампиньоны\'),
            (57, \'Бакинские огурцы\', \'просто бакинские огурцы\'),
            (58, \'Телячий язык\', \'просто телячий язык\'),
            (59, \'Кинза\', \'Кинза\'),
            (60, \'Петрушка\', \'просто петрушка\'),
            (61, \'Латук\', \'просто латук\'),
            (62, \'Российский сыр\', \'просто российский сыр\'),
            (64, \'Черный душистый перец\', \'просто черный душистый перец\');
            
            -- --------------------------------------------------------
            
            --
            -- Структура таблицы `product_set`
            --
            
            CREATE TABLE IF NOT EXISTS `product_set` (
              `id` int(11) NOT NULL,
              `name` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
              `visible` tinyint(1) DEFAULT \'0\'
            ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
            
            --
            -- Дамп данных таблицы `product_set`
            --
            
            INSERT INTO `product_set` (`id`, `name`, `visible`) VALUES
            (4, \'Сытный набор №1\', 1),
            (5, \'Диетический набор №1\', 1);
            
            -- --------------------------------------------------------
            
            --
            -- Структура таблицы `product_with_detail`
            --
            
            CREATE TABLE IF NOT EXISTS `product_with_detail` (
              `user_id` int(11) NOT NULL,
              `detail_id` int(11) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
            
            --
            -- Дамп данных таблицы `product_with_detail`
            --
            
            INSERT INTO `product_with_detail` (`user_id`, `detail_id`) VALUES
            (6, 3),
            (6, 4),
            (6, 5),
            (6, 6),
            (6, 7),
            (7, 4),
            (7, 7),
            (7, 8),
            (7, 9),
            (7, 10),
            (7, 11),
            (7, 12),
            (7, 13),
            (7, 16),
            (8, 4),
            (8, 7),
            (8, 17),
            (8, 18),
            (8, 19),
            (8, 20),
            (8, 21),
            (8, 22),
            (8, 23),
            (9, 24),
            (9, 25),
            (9, 26),
            (10, 4),
            (10, 7),
            (10, 23),
            (10, 27),
            (10, 28),
            (10, 29),
            (10, 30),
            (10, 31),
            (10, 32),
            (10, 33),
            (10, 34),
            (10, 35),
            (10, 36),
            (10, 37),
            (11, 4),
            (11, 7),
            (11, 23),
            (11, 35),
            (11, 39),
            (11, 40),
            (11, 41),
            (11, 42),
            (11, 43),
            (11, 44),
            (11, 45),
            (12, 7),
            (12, 11),
            (12, 16),
            (12, 23),
            (12, 26),
            (12, 44),
            (12, 46),
            (12, 47),
            (12, 49),
            (12, 51),
            (12, 52),
            (12, 53),
            (12, 54),
            (12, 55),
            (13, 4),
            (13, 7),
            (13, 8),
            (13, 16),
            (13, 23),
            (13, 52),
            (13, 53),
            (13, 56),
            (13, 57),
            (13, 58),
            (13, 59),
            (13, 60),
            (13, 61),
            (13, 62),
            (13, 64);
            
            -- --------------------------------------------------------
            
            --
            -- Структура таблицы `set_with_product`
            --
            
            CREATE TABLE IF NOT EXISTS `set_with_product` (
              `set_id` int(11) NOT NULL,
              `product_id` int(11) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
            
            --
            -- Дамп данных таблицы `set_with_product`
            --
            
            INSERT INTO `set_with_product` (`set_id`, `product_id`) VALUES
            (4, 7),
            (4, 12),
            (5, 11),
            (5, 12);
            
            --
            -- Индексы сохранённых таблиц
            --
            
            --
            -- Индексы таблицы `fos_user`
            --
            ALTER TABLE `fos_user`
              ADD PRIMARY KEY (`id`),
              ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
              ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`);
            
            --
            -- Индексы таблицы `migration_versions`
            --
            ALTER TABLE `migration_versions`
              ADD PRIMARY KEY (`version`);
            
            --
            -- Индексы таблицы `product`
            --
            ALTER TABLE `product`
              ADD PRIMARY KEY (`id`);
            
            --
            -- Индексы таблицы `product_detail`
            --
            ALTER TABLE `product_detail`
              ADD PRIMARY KEY (`id`);
            
            --
            -- Индексы таблицы `product_set`
            --
            ALTER TABLE `product_set`
              ADD PRIMARY KEY (`id`);
            
            --
            -- Индексы таблицы `product_with_detail`
            --
            ALTER TABLE `product_with_detail`
              ADD PRIMARY KEY (`user_id`,`detail_id`),
              ADD KEY `IDX_D95B2980A76ED395` (`user_id`),
              ADD KEY `IDX_D95B2980D8D003BB` (`detail_id`);
            
            --
            -- Индексы таблицы `set_with_product`
            --
            ALTER TABLE `set_with_product`
              ADD PRIMARY KEY (`set_id`,`product_id`),
              ADD KEY `IDX_17E795ED10FB0D18` (`set_id`),
              ADD KEY `IDX_17E795ED4584665A` (`product_id`);
            
            --
            -- AUTO_INCREMENT для сохранённых таблиц
            --
            
            --
            -- AUTO_INCREMENT для таблицы `fos_user`
            --
            ALTER TABLE `fos_user`
              MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
            --
            -- AUTO_INCREMENT для таблицы `product`
            --
            ALTER TABLE `product`
              MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
            --
            -- AUTO_INCREMENT для таблицы `product_detail`
            --
            ALTER TABLE `product_detail`
              MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
            --
            -- AUTO_INCREMENT для таблицы `product_set`
            --
            ALTER TABLE `product_set`
              MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
            --
            -- Ограничения внешнего ключа сохраненных таблиц
            --
            
            --
            -- Ограничения внешнего ключа таблицы `product_with_detail`
            --
            ALTER TABLE `product_with_detail`
              ADD CONSTRAINT `FK_D95B2980D8D003BB` FOREIGN KEY (`detail_id`) REFERENCES `product_detail` (`id`),
              ADD CONSTRAINT `FK_D95B2980A76ED395` FOREIGN KEY (`user_id`) REFERENCES `product` (`id`);
            
            --
            -- Ограничения внешнего ключа таблицы `set_with_product`
            --
            ALTER TABLE `set_with_product`
              ADD CONSTRAINT `FK_17E795ED4584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
              ADD CONSTRAINT `FK_17E795ED10FB0D18` FOREIGN KEY (`set_id`) REFERENCES `product_set` (`id`);
            
            /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
            /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
            /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;   
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
